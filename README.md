# Simulator Data Markers

## Start it

```node
npm i
npm run start
```
or 
```node
node ./src/index.js -p proximus -c ./data/proximus/markersConf.js -o ./src/proximus/fileOutput/proximusFakeData.xml -i 10 -l true
```
you can start it whithout any of the params, their are the default value

## How it works

It's a dual data simulator, it can be used for proximus or ipfl, you can run it by using the preconfigured params or use the node command to add your parameters

 -p for simulator wanted (proximus or ipfl)

 -c for path of the preconfigured markers you want to test (name, time and period)

 -o for path you want output file to be exported 

 -i for time execution of code (interval for each added marker will depend of this time and the number of marker )

 -l is for the console to be writed or not (if you just want to create the file or want to look at each marker who is added outside the exported file)

## How it structured

You have basically 3 folders, data for the source data to make fake marker, src where all the process is, and test (for ... test ?).

If you want to add or change something, please be carefull and use the test file to check if everything is still working, test are do with mocha.

In the src file you have 3 parts, the base where you can call all your simulator (just add it to the switch like the other),
a proximus folder and ipfl folder,
they basically have the same way of working but cause they don't have the same exported data and form of file,
we have to separate some part of the code.

All the function and mostly all the const are declared in externalised file to make it more easy to write and read

Inside the two simulator you can find : 

- fileBase where the base and required file are needed, you stock here :

    - possible name of action

    - part of match

    - form of marker 

    - exemple of wanted output (if the format is to hard to reproduce don't rewrite all the file just get it and write inside)

- fileOutput where the exported file have to go

- index.js where all the process is make

## How it works

In short, how the code is structured 

1- The first part is for preconfigured the output file structure (if needed)

2 - The second is for get and parse in Json Object the data of markers wanted to be add (the parse depend on the format, you can parse a json or xml file)

3 - The third part is for create the marker who will be added, you can use faker for the not really needed data, instead you have all you need for id, action, matchpart, time of marker in a match period

4 - The forth part is where each of the pregenerated markers will be add, based on the interval, you have to look how the file is structured to correctly add each markers, just take a look on AddMarkers func and change the form for your new simulator

5 - The last part is where the exported fake markers are exported, parsed like the data providers and ready to be use

