/* eslint-disable no-console */
/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
const assert = require('assert');

const { Proximus } = require('../src/proximus');

const should = require('should');

//  -------------- Required Modules --------------
const { fs, parser, growl, delay } = require('../src/utils/require');

const { program } = require('./programConf/program_test_proximus');

//  -------------- Getting the wanted base data xml --------------
const {
  readProximusFileXml,
  markersConf,
  getProximusFileBaseXml,
  getProximusFileBaseJson,
  getProximusDataJson,
} = require('../src/proximus/utils/readfileProximus');

// --------------- Getting Hard Needed Data (base declaration and timer format) ------------
let { exportFormat } = require('../src/utils/hardData');

const { valuePathProximus } = require('../src/proximus/utils/pathProximus');

// ------------ Set and Get Propreties inner value -------------
const { setValue, getValue } = require('../src/utils/objectProperties');

growl('Test is started');

describe('Path to file', () => {
  it('should return the path for the proximus marker', () => {
    assert.equal(
      getProximusFileBaseXml('Proximus_markers'),
      './src/proximus/fileBase/Proximus_markers.xml',
    );
  });
  it('should return the path for the proximus base file', () => {
    assert.equal(getProximusFileBaseXml('Proximus'), './src/proximus/fileBase/Proximus.xml');
  });
  it('should return the proximus marker file', () => {
    assert.equal(
      getProximusFileBaseJson('markersTypes'),
      './src/proximus/fileBase/markersTypes.json',
    );
  });
  it('should return the path for the part of a match for proximus', () => {
    assert.equal(getProximusFileBaseJson('matchPart'), './src/proximus/fileBase/matchPart.json');
  });
  it('should return the path of markers who will be added by the script', () => {
    assert.equal(getProximusDataJson('markersConf'), './data/proximus/markersConf.json');
  });
});

describe('File exist ?', () => {
  it('should return if the file proximus_marker exist for the given road', () => {
    should.exist(fs.readFileSync(getProximusFileBaseXml('Proximus_markers'), 'utf8'));
  });
  it('should return if the file proximus exist for the given road', () => {
    should.exist(fs.readFileSync(getProximusFileBaseXml('Proximus'), 'utf8'));
  });
  it('should return if the file markerTypes exist for the given road', () => {
    should.exist(fs.readFileSync(getProximusFileBaseJson('markersTypes'), 'utf8'));
  });
  it('should return if the file matchPart exist for the given road', () => {
    should.exist(fs.readFileSync(getProximusFileBaseJson('matchPart'), 'utf8'));
  });
  it('should return if the file markersConf exist for the given road', () => {
    should.exist(fs.readFileSync(getProximusDataJson('markersConf'), 'utf8'));
  });
});

describe('Is Object Correct and Setted', () => {
  it('should return a object for proximus marker correctly structured', () => {
    const matchAction = parser.xmlToJson(
      readProximusFileXml('Proximus_markers'),
      (err, json) => json,
    );
    matchAction.should.be.Object().and.have.property('CompetitionMatchAction');
  });
  it('should return a object for proximus output file correctly structured', () => {
    const getXmlAndSetNewJsExport = parser.xmlToJson(
      readProximusFileXml('Proximus'),
      (err, json) => {
        exportFormat = json;
        setValue(valuePathProximus, {}, exportFormat);
        return true;
      },
    );
    getXmlAndSetNewJsExport.should.be.equal(true);
    exportFormat.should.have.property('WebResultOfCompetitionMatchCollection');
  });
  it('should return a object for proximus with no action', () => {
    const action = getValue(valuePathProximus, exportFormat);
    action.should.be.Object().and.be.deepEqual({});
  });
});

describe('Array Is Getted And Info are Setted', () => {
  it('should return a array with markers', () => {
    markersConf.should.be.Array();
  });
});

describe('Integrated tests', () => {
  it('should return a xml file in the chosen location, currently not working need the await on the fonction', async () => {
    program.parse(process.argv);
    program.output = './test/temp/proximus_chosen_location_test.xml';

    if (fs.existsSync(program.output)) {
      fs.unlinkSync(program.output);
    }

    await delay(parseInt(program.interval, 10) + 500);

    proximus1 = new Proximus();
    proximus1.generateProximusXmlFile(null, program);

    await delay(parseInt(program.interval, 10) + 1000);

    should.exist(fs.readFileSync(program.output, 'utf8'), 'utf8');
  }).timeout(program.interval + 1000);

  it('should return a xml with a action "goal"', async () => {
    program.output = './test/temp/proximus_return_goal.xml';
    const marker = {
      markersConf: [
        {
          name: 'goal',
          timeFromLast: 120,
          period: 1,
        },
      ],
    };

    program.parse(process.argv);

    if (fs.existsSync(program.output)) {
      fs.unlinkSync(program.output);
    }

    await delay(parseInt(program.interval, 10) + 500);

    proximus2 = new Proximus();
    proximus2.generateProximusXmlFile(marker, program);

    await delay(parseInt(program.interval, 10) + 1000);

    const { CompetitionMatchAction } = parser.xmlToJson(
      fs.readFileSync(`${program.output}`, 'utf8'),
      (err, json) => getValue(valuePathProximus, json),
    );

    CompetitionMatchAction.should.have
      .property('CompetitionMatchActionType')
      .and.have.property('ActionType')
      .and.be.equal(marker.markersConf[0].name);
    CompetitionMatchAction.should.have
      .property('CompetitionMatchActionPart')
      .and.have.property('Id')
      .and.be.equal(marker.markersConf[0].period.toString());
  }).timeout(program.interval + 1000);
});
