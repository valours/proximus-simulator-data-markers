const { program } = require('../../src/utils/require');

program
  .option('-p, --parser <parser>', 'which parser to launch', 'proximus')
  .option('-c, --conf <conf>', 'path to the markers configuration')
  .option('-o, --output <output>', 'path to the output file', './test/temp/proximus_test.xml')
  .option('-i, --interval <interval>', 'interval between each markers in seconds', '1')
  .option('-l, --log <log>', 'log of the data into the console', false);

module.exports = {
  program,
};
