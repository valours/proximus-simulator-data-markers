/* eslint-disable no-console */
/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
const assert = require('assert');

const { Ipfl } = require('../src/ipfl');

const should = require('should');

//  -------------- Required Modules --------------
const { fs, growl, delay } = require('../src/utils/require');

const { program } = require('./programConf/program_test_ipfl');

//  -------------- Getting the wanted base data json --------------
const {
  getIPFLFileBaseJson,
  getIPFLDataJson,
  markersConf,
} = require('../src/ipfl/utils/readfileIPFL');

growl('Test is started');

describe('Path to file', () => {
  it('should return the path for the ipfl marker', () => {
    assert.equal(
      getIPFLFileBaseJson('IPFL_markers_period'),
      './src/ipfl/fileBase/IPFL_markers_period.json',
    );
  });
  it('should return the path for the ipfl base file', () => {
    assert.equal(getIPFLFileBaseJson('IPFL'), './src/ipfl/fileBase/IPFL.json');
  });
  it('should return the ipfl marker file', () => {
    assert.equal(getIPFLFileBaseJson('markersTypes'), './src/ipfl/fileBase/markersTypes.json');
  });
  it('should return the path for the part of a match for ipfl', () => {
    assert.equal(getIPFLFileBaseJson('matchPart'), './src/ipfl/fileBase/matchPart.json');
  });
  it('should return the path of markers who will be added by the script', () => {
    assert.equal(getIPFLDataJson('markersConf'), './data/ipfl/markersConf.json');
  });
});

describe('File exist ?', () => {
  it('should return if the file proximus_marker exist for the given road', () => {
    should.exist(fs.readFileSync(getIPFLFileBaseJson('IPFL_markers_period'), 'utf8'));
  });
  it('should return if the file ipfl exist for the given road', () => {
    should.exist(fs.readFileSync(getIPFLFileBaseJson('IPFL'), 'utf8'));
  });
  it('should return if the file markerTypes exist for the given road', () => {
    should.exist(fs.readFileSync(getIPFLFileBaseJson('markersTypes'), 'utf8'));
  });
  it('should return if the file matchPart exist for the given road', () => {
    should.exist(fs.readFileSync(getIPFLFileBaseJson('matchPart'), 'utf8'));
  });
  it('should return if the file markersConf exist for the given road', () => {
    should.exist(fs.readFileSync(getIPFLDataJson('markersConf'), 'utf8'));
  });
});

describe('Array Is Getted And Info are Setted', () => {
  it('should return a array with markers', () => {
    markersConf.should.be.Array();
  });
});

describe('Integrated tests', () => {
  it('should return a json file in the chosen location, currently not working need the await on the fonction', async () => {
    program.parse(process.argv);
    program.output = './test/temp/ipfl_chosen_location_test.json';

    if (fs.existsSync(program.output)) {
      fs.unlinkSync(program.output);
    }

    await delay(parseInt(program.interval, 10) + 500);

    ipfl1 = new Ipfl();
    ipfl1.generateIPFLJsonFile(null, program);

    await delay(parseInt(program.interval, 10) + 1000);

    should.exist(fs.readFileSync(program.output, 'utf8'), 'utf8');
  }).timeout(program.interval + 1000);

  it('should return a json with a action "goal"', async () => {
    program.output = './test/temp/ipfl_return_goal.json';
    const marker = {
      markersConf: [
        {
          name: 'goal',
          timeFromLast: 120,
          period: '1st half start',
        },
      ],
    };

    program.parse(process.argv);

    if (fs.existsSync(program.output)) {
      fs.unlinkSync(program.output);
    }

    await delay(parseInt(program.interval, 10) + 500);

    ipfl2 = new Ipfl();
    ipfl2.generateIPFLJsonFile(marker, program);

    await delay(parseInt(program.interval, 10) + 1000);

    const { data } = JSON.parse(fs.readFileSync(`${program.output}`, 'utf8'));

    data[0].should.have
      .property('info')
      .and.have.property('action_name')
      .and.be.equal(marker.markersConf[0].name);
    data[0].should.have
      .property('info')
      .and.have.property('half')
      .and.be.equal(marker.markersConf[0].period.toString());
  }).timeout(program.interval + 1000);
});
