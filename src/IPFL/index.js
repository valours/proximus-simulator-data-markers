/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
//  -------------- Required Modules --------------
const { cloneDeep } = require('../utils/require');

//  -------------- Getting the wanted base data xml --------------
const {
  markersTypes,
  matchPart,
  markersConf,
  readIpflJsonFile,
  defaultExportPath,
} = require('./utils/readfileIPFL');

// --------------- Getting Hard Needed Data (base declaration and timer format) ------------

const { RandomPlayer, arrayOfActionsProperties } = require('./utils/hardDataIPFL');

const { intervalFormat } = require('../utils/hardData');

//  --------------  const for the simulator --------------
const { teamIs } = require('../utils/changeableData');

// ------------ Set and Get Propreties inner value -------------
const {
  setValue,
  isTrue,
  isExistAndNumber,
  isExistAndString,
  isExistAndObject,
} = require('../utils/objectProperties');

const { logInfoIPFL } = require('../utils/logInfo');

const { GetActionInfo, GetMatchPart } = require('../utils/markerData');

const { addMarkersIPFL } = require('./utils/ProcessIpfl');

class Ipfl {
  constructor() {
    this.gettedActions = '';
    this.timeAction = 0;
    this.actualTime = 0;
    this.lastHalf = '1st half start';
  }
  /** Generate a proximus fake xml file of actions (markers) with presetted value, if no args are referenced the code will get the base data value
   * @param {Object} inputMarkers - Object marker wanted to be parsed, if nothing is referenced, the data in the markersConf.json will be used
   * @param {String} path - Path for the outside xml file location, if nothing is referenced, the data in the readfileProximus will be used
   * @param {Number} intervalMarker - Interval in second for the SetTimeOut, define when the markers will be add one after in the xml file,
   *  if nothing is referenced, the data in the changeableData.js will be used
   * @param {Boolean} logEnable - logEnable allow the marker to be writed in the console or log
   */
  generateIPFLJsonFile(inputMarkers, { output: path, interval: intervalMarker, log: logEnable }) {
    const matchAction = JSON.parse(readIpflJsonFile('IPFL'));

    // --------------  Mocked Marker (for First and second period) --------------//
    const markers = isExistAndObject(inputMarkers) ? inputMarkers.markersConf : markersConf;
    const interval = isExistAndNumber(intervalMarker);
    const logIsEnabled = isTrue(logEnable);

    markers
      .map((marker) => {
        const actionInfo = GetActionInfo(marker.name, markersTypes);
        const matchInfo = GetMatchPart(marker.period, matchPart);
        this.timeAction = marker.timeFromLast;
        const playerInfo = RandomPlayer();

        if (this.lastHalf !== matchInfo.id) {
          this.actualTime = this.timeAction;
          this.lastHalf = matchInfo.id;
        } else {
          this.actualTime += this.timeAction;
        }

        const arrayOfDataForActionsProperties = [
          (parseInt(matchAction.id, 10) + 1).toString(),
          (parseInt(matchAction.id, 10) + 1).toString(), // Cause we have 2 id to set
          actionInfo.id,
          actionInfo.name,
          playerInfo.player_id,
          playerInfo.player_name,
          teamIs[Math.round(Math.random())],
          playerInfo.team_name,
          playerInfo.position_id,
          playerInfo.position_name,
          playerInfo.opponent_id,
          playerInfo.opponent_name,
          teamIs[Math.round(Math.random())],
          playerInfo.oppponent_team_name,
          playerInfo.opponent_position_id,
          playerInfo.opponent_position_name,
          matchInfo.name,
          this.actualTime,
          playerInfo.zone_id,
          playerInfo.zone_name,
          playerInfo.pos_x,
          playerInfo.pos_y,
          playerInfo.standart_id,
          playerInfo.standart_name,
          playerInfo.possession_id,
          playerInfo.possession_name,
          playerInfo.possession_team_id,
          playerInfo.possession_team_name,
          playerInfo.possession_time,
          playerInfo.possession_number,
          playerInfo.attack_status_id,
          playerInfo.attack_status_name,
          playerInfo.attack_type_id,
          playerInfo.attack_type_name,
          playerInfo.attack_flang_id,
          playerInfo.attack_flang_name,
          playerInfo.attack_team_id,
          playerInfo.attack_team_name,
          playerInfo.attack_number,
          playerInfo.dl,
        ];

        arrayOfActionsProperties.map((ActionProperty, index) =>
          setValue(ActionProperty, arrayOfDataForActionsProperties[index], matchAction),
        );

        return cloneDeep(matchAction);
      })
      .map((action, index) =>
        setTimeout(() => {
          this.gettedActions = addMarkersIPFL(
            action,
            this.gettedActions,
            isExistAndString(path) ? path : defaultExportPath,
          );
          if (logIsEnabled) {
            logInfoIPFL(action, interval, index, markers.length);
          }
        }, (interval * intervalFormat + index * interval * intervalFormat) / markers.length),
      );
  }
}
module.exports = { Ipfl };
