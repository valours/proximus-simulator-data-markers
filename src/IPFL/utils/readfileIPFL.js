const { fs } = require('../../utils/require');

const getIPFLFileBaseJson = (filename) => `./src/ipfl/fileBase/${filename}.json`;

const getIPFLDataJson = (filename) => `./data/ipfl/${filename}.json`;

const getIPFLFileOutJson = (filename) => `./src/ipfl/fileOutput/${filename}.json`;

const createAndReadJsonOut = () => {
  if (!fs.existsSync(getIPFLFileOutJson('ipfl'))) {
    fs.writeFileSync(getIPFLFileOutJson('ipfl'));
  }
  return fs.readFileSync(getIPFLFileOutJson('ipfl'));
};

const verifyAndReadIpflDataJson = (filepath) => {
  if (fs.existsSync(filepath, 'utf8')) {
    return fs.readFileSync(filepath, 'utf8');
  }
  return fs.readFileSync(getIPFLDataJson('markersConf'), 'utf8');
};
const readIpflJsonFile = (filename) => fs.readFileSync(getIPFLFileBaseJson(filename), 'utf8');
const { markersTypes } = JSON.parse(fs.readFileSync(getIPFLFileBaseJson('markersTypes'), 'utf8'));
const { matchPart } = JSON.parse(fs.readFileSync(getIPFLFileBaseJson('matchPart'), 'utf8'));
const { markersConf } = JSON.parse(fs.readFileSync(getIPFLDataJson('markersConf'), 'utf8'));
const defaultExportPath = './src/ipfl/fileOutput/ipfl.json';

module.exports = {
  readIpflJsonFile,
  createAndReadJsonOut,
  markersTypes,
  matchPart,
  markersConf,
  defaultExportPath,
  getIPFLFileBaseJson,
  getIPFLDataJson,
  getIPFLFileOutJson,
  verifyAndReadIpflDataJson,
};
