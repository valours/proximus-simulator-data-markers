/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
const { fs } = require('../../utils/require');

let { sendsActions, exportFormat } = require('../../utils/hardData');

const { oneValueGettedFormatIPFL, multipleValueGettedFormatIPFL } = require('./pathIPFL');

const addMarkersIPFL = (marker, presendAction, path) => {
  if (presendAction) {
    presendAction.data.push(marker);
    sendsActions = JSON.stringify(presendAction.data);
    exportFormat = JSON.parse(multipleValueGettedFormatIPFL(sendsActions));
    // setValue(valuePathIPFL, JSON.parse(multipleValueGettedFormatIPFL(sendsActions)), ipflFile);
  } else {
    sendsActions = JSON.stringify(marker);
    exportFormat = JSON.parse(oneValueGettedFormatIPFL(sendsActions));
    // setValue(valuePathIPFL, JSON.parse(oneValueGettedFormatIPFL(sendsActions)), ipflFile);
  }

  // --------------  Exporting the builed output json --------------//

  fs.writeFileSync(path, JSON.stringify(exportFormat));

  if (presendAction) {
    return JSON.parse(multipleValueGettedFormatIPFL(sendsActions));
  }
  return JSON.parse(oneValueGettedFormatIPFL(sendsActions));
};

module.exports = {
  addMarkersIPFL,
};
