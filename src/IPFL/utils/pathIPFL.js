const valuePathIPFL = 'data';
const multipleValueGettedFormatIPFL = (sendsActions) => `{"data":${sendsActions}}`;
const oneValueGettedFormatIPFL = (sendsActions) => `{"data":[${sendsActions}]}`;

module.exports = {
  valuePathIPFL,
  oneValueGettedFormatIPFL,
  multipleValueGettedFormatIPFL,
};
