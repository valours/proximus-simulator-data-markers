/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
const { program } = require('./utils/programConf');
// const { GetFileFromUrl } = require('./utils/getUrlFile');

const { Proximus } = require('./proximus');
const { verifyAndReadProximusDataJson } = require('./proximus/utils/readfileProximus');

const { Ipfl } = require('./ipfl');
const { verifyAndReadIpflDataJson } = require('./ipfl/utils/readfileIPFL');

program.parse(process.argv);

console.log(
  `parser params - name of script : ${program.parser} \n path to data file : ${
    program.conf
  } \n path to exported file and format : ${program.output} \n script duration : ${
    program.interval
  } \n log is enabled : ${program.log}`,
);

// const file = GetFileFromUrl(program.conf); // Can be used if really needed, but need to make all the convert part for the file

switch (program.parser) {
  case 'proximus':
    new Proximus().generateProximusXmlFile(
      JSON.parse(verifyAndReadProximusDataJson(program.conf)),
      program,
    ); // We can put args, like the name of the wantedFile (the markerConf File)
    break;
  case 'ipfl':
    new Ipfl().generateIPFLJsonFile(JSON.parse(verifyAndReadIpflDataJson(program.conf)), program);
    break;
  default:
    console.log("this script name doesn't exist");
    break;
}
