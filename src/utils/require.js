const fs = require('fs');
const xmlToJson = require('xml-to-json-stream');

const parser = xmlToJson({ attributeMode: true });
const moment = require('moment-timezone'); // moment-timezone
const convert = require('xml-js');
const objectPath = require('object-path');
const cloneDeep = require('lodash.clonedeep');
const faker = require('faker');
const delay = require('delay');
const program = require('commander');
const growl = require('growl');
const pino = require('pino');

const logger = pino({
  prettyPrint: true,
});

module.exports = {
  fs,
  parser,
  moment,
  convert,
  objectPath,
  cloneDeep,
  delay,
  faker,
  program,
  growl,
  logger,
  pino,
};
