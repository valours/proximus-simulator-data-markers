/* eslint-disable no-console */

const GetActionInfo = (name, markerType) => {
  const action = markerType.find((markerName) => markerName.name === name);
  return action;
};

const GetMatchPart = (id, actionMatchPart) => {
  if (typeof id === 'number') {
    const part = actionMatchPart.find((match) => match.id === id);
    return part;
  } else if (typeof id === 'string') {
    const part = actionMatchPart.find((match) => match.name === id);
    return part;
  }
  return console.error('Expect id to be int or string');
};

module.exports = {
  GetActionInfo,
  GetMatchPart,
};
