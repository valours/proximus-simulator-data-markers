const { program } = require('./require');

program
  .option('-p, --parser <parser>', 'which parser to launch', 'proximus')
  .option(
    '-c, --conf <conf>',
    'path to the markers configuration',
    'https://cdnapi.bamboo-video.com/api/football/marker/?format=json&iid=573881b7181f46ae4c8b4567&filter={%22matchid%22:1466472}&videoMarkers=true',
  )
  .option('-o, --output <output>', 'path to the output file')
  .option('-i, --interval <interval>', 'interval between each markers in seconds')
  .option('-l, --log <log>', 'log of the data into the console', 'true');

module.exports = {
  program,
};
