/* eslint-disable no-console */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-restricted-globals */
const { objectPath, fs } = require('./require');
const { interval } = require('./changeableData');

const setValue = (propertyPath, value, obj) => objectPath.set(obj, propertyPath, value);
const getValue = (propertyPath, obj) => objectPath.get(obj, propertyPath, null); // null is a default value, returns undefined if nothing set

const isString = (value) => typeof value === 'string' || value instanceof String;

const isObject = (value) => value && typeof value === 'object' && value.constructor === Object;

const isNumber = (value) => typeof value === 'number' && isFinite(value);

const isTrue = (value) => value === 'true';

const isExistAndNumber = (intervalWanted) => {
  if (isNumber(parseInt(intervalWanted, 10))) {
    return intervalWanted;
  }
  return interval;
};

const isExistAndString = (path) => {
  if (isString(path)) {
    if (
      fs.existsSync(
        path
          .split('\\')
          .slice(0, -1)
          .join('/'),
      ) ||
      fs.existsSync(
        path
          .split('/')
          .slice(0, -1)
          .join('/'),
      )
    ) {
      return path;
    }
  }
  return false;
};

const isExistAndObject = (inputmarkers) => {
  if (isObject(inputmarkers) && Array.isArray(inputmarkers.markersConf)) {
    return inputmarkers.markersConf;
  }
  return false;
};

module.exports = {
  setValue,
  getValue,
  isString,
  isObject,
  isTrue,
  isExistAndNumber,
  isExistAndString,
  isExistAndObject,
};
