/* eslint-disable no-console */
const { XMLHttpRequest } = require('xmlhttprequest');

const GetFileFromUrl = (yourUrl) => {
  const Httpreq = new XMLHttpRequest(); // a new request
  Httpreq.open('GET', yourUrl, false);
  Httpreq.send(null);
  return Httpreq.responseText;
};

module.exports = { GetFileFromUrl };
