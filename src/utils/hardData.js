const sendsActions = '';
const gettedActions = '';
const intervalFormat = 1000; // format de l'interval, temps = interval * intervalFormat, 1000 * 5 = 5 secondes
const actualTime = 0;
const exportFormat = {};

const options = {
  compact: true,
  ignoreComment: true,
  spaces: 4,
};

module.exports = {
  sendsActions,
  gettedActions,
  intervalFormat,
  actualTime,
  exportFormat,
  options,
};
