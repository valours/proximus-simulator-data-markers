const timezone = 'Europe/Paris'; //  Look at moment timezone maps, take your country and the city
const teamIs = ['503', '553'];
const interval = 2; // interval d'injection des markers dans le xml et dans la console
module.exports = {
  timezone,
  teamIs,
  interval,
};
