/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
const { moment } = require('./require');
const { intervalFormat } = require('./hardData');
const { pino } = require('./require');

const loggerProximus = pino(
  {
    prettyPrint: { colorize: false, translateTime: true, ignore: 'hostname,pid' },
  },
  pino.destination('./src/logs/proximus.log'),
);

const loggerIpfl = pino(
  {
    prettyPrint: { colorize: false, translateTime: true, ignore: 'hostname,pid' },
  },
  pino.destination('./src/logs/ipfl.log'),
);

const logInfoProximus = (action, interval, index, file, length) =>
  loggerProximus.info(
    `'N°:' ${index + 1}`,
    `'Tps:' ${(interval * intervalFormat + index * interval * intervalFormat) / 1000 / length} 's `,
    `GMT1:' ${
      file.WebResultOfCompetitionMatchCollection.Result.CompetitionMatch.GMTDateStartPeriod1
    }`,
    `'GMT2:' ${
      file.WebResultOfCompetitionMatchCollection.Result.CompetitionMatch.GMTDateStartPeriod2
    }`,
    `'Id:' ${action.Id}`,
    `'TpsM:' ${moment(action.GMTDate).format('YYYY-MM-DDTHH:mm:ss')}`,
  );

const logInfoIPFL = (action, interval, index, length) =>
  loggerIpfl.info(
    `'N°:' ${index + 1} `,
    `'Tps:' ${(interval * intervalFormat + index * interval * intervalFormat) / 1000 / length} 's `,
    `Part:' ${action.info.half} `,
    `'Id:' ${action.id} `,
    `'TpsM:' ${action.info.second}`,
  );

module.exports = {
  logInfoProximus,
  logInfoIPFL,
};
