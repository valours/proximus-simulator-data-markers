const { moment } = require('./require');
const { timezone } = require('./changeableData');

const momentFormatMarker = (date, timeAction) =>
  moment(date)
    .tz(timezone)
    .utc(date)
    .add(timeAction, 's')
    .format('YYYY-MM-DDTHH:mm:ss');

const momentFormatGMT = (date, timeAction) =>
  moment(date)
    .tz(timezone)
    .utc(date)
    .add(timeAction, 'm')
    .format('YYYY-MM-DDTHH:mm:ss');

module.exports = {
  momentFormatMarker,
  momentFormatGMT,
};
