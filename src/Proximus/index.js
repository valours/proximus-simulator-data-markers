/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
//  -------------- Required Modules --------------
const { parser, cloneDeep, moment } = require('../utils/require');

//  -------------- Getting the wanted base data xml --------------
const {
  readProximusFileXml,
  markersTypes,
  matchPart,
  markersConf,
  defaultExportPath,
} = require('./utils/readfileProximus');

// --------------- Getting Hard Needed Data (base declaration and timer format) ------------
let { exportFormat } = require('../utils/hardData');

const { intervalFormat } = require('../utils/hardData');

const { GMT, arrayOfActionsProperties, RandomPlayer } = require('./utils/hardProximusData');

const { valuePathProximus } = require('./utils/pathProximus');

//  --------------  const for the simulator --------------
const { teamIs } = require('../utils/changeableData');

// ------------- momentFormat ------------------
const { momentFormatMarker } = require('../utils/moment');

// ------------ Set and Get Propreties inner value -------------
const {
  setValue,
  isTrue,
  isExistAndString,
  isExistAndNumber,
  isExistAndObject,
} = require('../utils/objectProperties');

const { logInfoProximus } = require('../utils/logInfo');

const { declareGMT, addMarkersProximus } = require('./utils/ProcessProximus');

const { GetActionInfo, GetMatchPart } = require('../utils/markerData');

class Proximus {
  constructor() {
    this.gettedActions = '';
    this.timeAction = 0;
    this.actualDateInput = moment();
    this.GMT = GMT;
    this.lastHalf = 1;
  }
  /** Generate a proximus fake xml file of actions (markers) with presetted value, if no args are referenced the code will get the base data value
   * @param {Object} inputMarkers - Object marker wanted to be parsed, if nothing is referenced, the data in the markersConf.json will be used
   * @param {String} path - Path for the outside xml file location, if nothing is referenced, the data in the readfileProximus will be used
   * @param {Number} intervalMarker - Interval in second for the SetTimeOut, define when the markers will be add one after in the xml file,
   *  if nothing is referenced, the data in the changeableData.js will be used
   * @param {Boolean} logEnable - logEnable allow the marker to be writed in the console or log
   */
  generateProximusXmlFile(
    inputMarkers,
    { output: path, interval: intervalMarker, log: logEnable },
  ) {
    if (readProximusFileXml) {
      const matchAction = parser.xmlToJson(
        readProximusFileXml('Proximus_markers'),
        (err, json) => json.CompetitionMatchAction,
      );

      const getXmlAndSetNewJsExport = parser.xmlToJson(
        readProximusFileXml('Proximus'),
        (err, json) => {
          exportFormat = json;
          setValue(valuePathProximus, {}, exportFormat);
          return true;
        },
      );

      // --------------  Mocked Marker (for First and second period) --------------//
      if (getXmlAndSetNewJsExport) {
        const markers = isExistAndObject(inputMarkers) ? inputMarkers.markersConf : markersConf;
        const interval = isExistAndNumber(intervalMarker);
        const logIsEnabled = isTrue(logEnable);
        markers
          .map((marker) => {
            const actionInfo = GetActionInfo(marker.name, markersTypes);
            const matchInfo = GetMatchPart(marker.period, matchPart);
            this.timeAction = marker.timeFromLast;
            const playerInfo = RandomPlayer();

            if (this.lastHalf !== matchInfo.id) {
              this.actualDateInput = momentFormatMarker(
                GMT.CompetitionMatch.GMTDateStartPeriod2,
                this.timeAction,
              );
              this.lastHalf = matchInfo.id;
            } else {
              this.actualDateInput = momentFormatMarker(this.actualDateInput, this.timeAction);
            }

            const arrayOfDataForActionsProperties = [
              (parseInt(matchAction.Id, 10) + 1).toString(),
              teamIs[Math.round(Math.random())],
              this.actualDateInput,
              playerInfo,
              actionInfo.id,
              actionInfo.name,
              matchInfo.id,
              matchInfo.name,
            ];

            arrayOfActionsProperties.map((ActionProperty, index) =>
              setValue(ActionProperty, arrayOfDataForActionsProperties[index], matchAction),
            );

            return cloneDeep(matchAction);
          })
          .forEach((action, index) =>
            setTimeout(() => {
              if (declareGMT(action.CompetitionMatchActionPart.Id, exportFormat)) {
                this.gettedActions = addMarkersProximus(
                  action,
                  this.gettedActions,
                  exportFormat,
                  isExistAndString(path) ? path : defaultExportPath,
                );
                if (logIsEnabled) {
                  logInfoProximus(action, interval, index, exportFormat, markers.length);
                }
              } else {
                console.error("ActionPart Id wasn't defined");
              }
            }, (interval * intervalFormat + index * interval * intervalFormat) / markers.length),
          );
      } else {
        console.error("Xml base File doesn't exist or an error occured during the parsing !");
      }
    } else {
      console.error("Xml base File doesn't exist, pls reference the base data providers file !");
    }
  }
}
module.exports = { Proximus };
