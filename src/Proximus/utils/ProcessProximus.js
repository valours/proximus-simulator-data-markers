/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
const { fs, convert } = require('../../utils/require');
const { setValue } = require('../../utils/objectProperties');
const { options } = require('../../utils/hardData');
const {
  GMT,
  period1,
  period2,
  period3,
  period4,
  halfTime,
  postGame,
} = require('./hardProximusData');
let { sendsActions } = require('../../utils/hardData');
const {
  valuePathProximus,
  GMTPath,
  oneValueGettedFormatProximus,
  multipleValueGettedFormatProximus,
} = require('./pathProximus');

const declareGMT = (period, proximusFile) => {
  switch (period) {
    case 1:
      setValue(`${GMTPath}.${period1}`, GMT.CompetitionMatch.GMTDateStartPeriod1, proximusFile);
      setValue(`${GMTPath}.${halfTime}`, GMT.CompetitionMatch.GMTDateHalfTime, proximusFile);
      return true;
    case 2:
      setValue(`${GMTPath}.${period2}`, GMT.CompetitionMatch.GMTDateStartPeriod2, proximusFile);
      setValue(`${GMTPath}.${postGame}`, GMT.CompetitionMatch.GMTDatePostGame, proximusFile);
      return true;
    case 3:
      setValue(`${GMTPath}.${period3}`, GMT.CompetitionMatch.GMTDateStartPeriod3, proximusFile);
      break;
    case 4:
      setValue(`${GMTPath}.${period4}`, GMT.CompetitionMatch.GMTDateStartPeriod4, proximusFile);
      return true;
    case 5:
      break;
    default:
      console.error("Marker wasn't correctly deffined in markersConf.json");
      return false;
  }
  return false;
};
const addMarkersProximus = (marker, presendAction, proximusFile, path) => {
  if (presendAction) {
    presendAction.CompetitionMatchAction.push(marker);
    sendsActions = JSON.stringify(presendAction.CompetitionMatchAction);
    setValue(
      valuePathProximus,
      JSON.parse(multipleValueGettedFormatProximus(sendsActions)),
      proximusFile,
    );
  } else {
    sendsActions = JSON.stringify(marker);
    setValue(
      valuePathProximus,
      JSON.parse(oneValueGettedFormatProximus(sendsActions)),
      proximusFile,
    );
  }

  // --------------  Json into Xml --------------//
  const result = convert.json2xml(JSON.parse(JSON.stringify(proximusFile)), options);

  // --------------  Exporting the builed output xml --------------//

  fs.writeFileSync(path, result);

  if (presendAction) {
    return JSON.parse(multipleValueGettedFormatProximus(sendsActions));
  }
  return JSON.parse(oneValueGettedFormatProximus(sendsActions));
};

module.exports = {
  declareGMT,
  addMarkersProximus,
};
