const { fs } = require('../../utils/require');

const getProximusFileBaseXml = (filename) => `./src/proximus/fileBase/${filename}.xml`;

const getProximusFileBaseJson = (filename) => `./src/proximus/fileBase/${filename}.json`;

const getProximusDataJson = (filename) => `./data/proximus/${filename}.json`;

const getProximusFileOutXml = (filename) => `./src/proximus/fileOutput/${filename}.xml`;

const readProximusFileXml = (filename) => fs.readFileSync(getProximusFileBaseXml(filename), 'utf8');

const verifyAndReadProximusDataJson = (filepath) => {
  if (fs.existsSync(filepath, 'utf8')) {
    return fs.readFileSync(filepath, 'utf8');
  }
  return fs.readFileSync(getProximusDataJson('markersConf'), 'utf8');
};

const createAndReadXmlOut = (filename) => {
  if (!fs.existsSync(getProximusFileOutXml(filename))) {
    fs.writeFileSync(getProximusFileOutXml(filename));
  }
  return fs.readFileSync(getProximusFileOutXml(filename));
};

const { markersTypes } = JSON.parse(
  fs.readFileSync(getProximusFileBaseJson('markersTypes'), 'utf8'),
);
const { matchPart } = JSON.parse(fs.readFileSync(getProximusFileBaseJson('matchPart'), 'utf8'));
const { markersConf } = JSON.parse(fs.readFileSync(getProximusDataJson('markersConf'), 'utf8'));
const defaultExportPath = './src/proximus/fileOutput/proximus.xml';

module.exports = {
  readProximusFileXml,
  createAndReadXmlOut,
  markersTypes,
  matchPart,
  markersConf,
  getProximusDataJson,
  getProximusFileBaseJson,
  verifyAndReadProximusDataJson,
  defaultExportPath,
  getProximusFileBaseXml,
};
