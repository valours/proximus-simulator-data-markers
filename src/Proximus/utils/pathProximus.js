const valuePathProximus = 'WebResultOfCompetitionMatchCollection.Result.CompetitionMatch.Actions';
const GMTPath = 'WebResultOfCompetitionMatchCollection.Result.CompetitionMatch';

const multipleValueGettedFormatProximus = (sendsActions) =>
  `{"CompetitionMatchAction":${sendsActions}}`;

const oneValueGettedFormatProximus = (sendsActions) =>
  `{"CompetitionMatchAction":[${sendsActions}]}`;

module.exports = {
  valuePathProximus,
  GMTPath,
  oneValueGettedFormatProximus,
  multipleValueGettedFormatProximus,
};
