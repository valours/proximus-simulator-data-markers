const { moment } = require('../../utils/require');
const { momentFormatGMT } = require('../../utils/moment');
const { timezone } = require('../../utils/changeableData');
const { faker } = require('../../utils/require');

const lastHalf = 1;
const rdmProlong1 = Math.round(Math.random() * 5 + 1);
const rdmProlong2 = Math.round(Math.random() * 5 + 1);
const period1 = 'GMTDateStartPeriod1';
const period2 = 'GMTDateStartPeriod2';
const period3 = 'GMTDateStartPeriod3';
const period4 = 'GMTDateStartPeriod4';
const halfTime = 'GMTDateHalfTime';
const postGame = 'GMTDatePostGame';

const GMT = {
  //  Comment it if the data are already referenced in the getted XML  and uncomment the other one in Xml parser under
  CompetitionMatch: {
    GMTDateHalfTime: momentFormatGMT(moment(), 45),
    GMTDatePostGame: momentFormatGMT(moment(), 105),
    GMTDateStartPeriod1: moment()
      .tz(timezone)
      .utc(moment())
      .format('YYYY-MM-DDTHH:mm:ss'),
    GMTDateStartPeriod2: momentFormatGMT(moment(), 60),
    GMTDateStartPeriod3: momentFormatGMT(moment(), 0 + rdmProlong1),
    GMTDateStartPeriod4: momentFormatGMT(moment(), 0 + rdmProlong2),
  },
};
const arrayOfActionsProperties = [
  'Id',
  'TeamId',
  'GMTDate',
  'Player',
  'CompetitionMatchActionType.Id',
  'CompetitionMatchActionType.ActionType',
  'CompetitionMatchActionPart.Id',
  'CompetitionMatchActionPart.Description',
];

const RandomPlayer = () => {
  const player = {
    Id: faker.random.number(),
    TeamId: 0,
    PositionId: Math.round(faker.random.number() / 1000),
    Height: Math.round(faker.random.number() / 1000),
    Weight: Math.round(faker.random.number() / 1000),
    IdalgoId: faker.random.number(),
    Birthdate: faker.date.past(),
    NickName: faker.name.lastName(),
    FirstName: faker.name.firstName(),
    Image: faker.image.avatar(),
    CountryFR: faker.address.country(),
    CountryNL: faker.address.country(),
    CityFR: faker.address.city(),
    CityNL: faker.address.city(),
    FacebookAccount: faker.internet.userName(),
    TwitterAccount: faker.internet.userName(),
    CountryCode: faker.address.countryCode(),
    NumShirt: Math.round(faker.random.number() / 1000),
    XPos: Math.round(faker.random.number() / 1000),
    YPos: Math.round(faker.random.number() / 1000),
    IsStarted: Math.random() >= 0.5,
  };
  return player;
};
module.exports = {
  GMT,
  lastHalf,
  arrayOfActionsProperties,
  RandomPlayer,
  period1,
  period2,
  period3,
  period4,
  halfTime,
  postGame,
};
